//
//  ViewController.swift
//  Eva3_1_Persistencia
//
//  Created by TEMPORAL2 on 17/11/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //arreglo de TextField sin longitud
    @IBOutlet var txtDatos: [UITextField]!
    //control click y vinculas los textfield al arreglo
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let urlFile = leerUbicacion()
        //verificamos que el archivo exista
        if NSFileManager.defaultManager().fileExistsAtPath(urlFile.path!) {
            //si existe, leer el contenido
            if let arr = NSArray(contentsOfURL: urlFile) as? [String] {
                for var i = 0; i < arr.count; i++ {
                    txtDatos[i].text = arr[i]
                }
            }
        }
        //vamos a decirle que hacer cuando la app se cierre
        //cuandi cierre vamos a ejecutar una funcion para guardar
        let app = UIApplication.sharedApplication()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationWillResignActive:", name: UIApplicationWillResignActiveNotification, object: app)
        
    }
    //la aplicacion abandona el estado activo
    func applicationWillResignActive (notification: NSNotification) {
        let urlFile = leerUbicacion()
        //leemos el arreglo de cuatros de texto y lo guardamos
        let arreglo = (txtDatos as NSArray).valueForKey("text") as! NSArray
        arreglo.writeToURL(urlFile, atomically: true)
        
        
    }
    
    func leerUbicacion() -> NSURL {
        //recupera la ruta de los documentos dentro del dominio como app del usuario y a esa ruta se le pega el nombre del archivo
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls.first!.URLByAppendingPathComponent("data.plist")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

